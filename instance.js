const puppeteer = require('puppeteer');
const config = require('config');
const instances = config.get('instances');
const axios = require('axios');
var timer = 600000;
var sys_id = "";
/*
default.json config ex.
instances: [{
  "name": "devinstance in servicenow",
  "user": "username",
  "pass": "password"
}]
*/
(async () => {
  while (true) {
    var error = false;
    console.log("Initiating")
    for (e of instances) {
      timer = 600000;
      do{
        const browser = await puppeteer.launch({args: ['--no-sandbox'], headless: true});
        const page = await browser.newPage();
        try {
          console.log("instance", e.name)
          await page.goto(e.name);
          await page.type("#user_name", e.user);
          await page.type("#user_password", e.pass); 
          await page.click('#sysverb_login');
          await page.waitForNavigation();
          error = false;
        } catch (e) {
          console.error("retrying cause error");
          console.error(e);
          timer = 15000;
          error = true;
        }
        axios.get("https://dev20292.service-now.com/api/97236/live_keeper/create", {headers: { 'Authorization' : 'Basic' + btoa(e.user+':'+e.pass}}))
             .then(function(response) {
                console.log(response);
             }).catch(function err() { console.log(error)});
        axios.post("https://dev20292.service-now.com/api/97236/live_keeper/delete", {sys_id: sys_id})
              .then(function(response){})
              .catch(function (err) {console.log(err)})
        await browser.close();
      }while (error)
    }
    console.log("entering sleep");
    await timeout(timer);
  }
})();

function timeout(ms) {
  return new Promise(resolve => setTimeout(resolve, ms))
}